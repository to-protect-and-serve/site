const h = window.hyperscript;

/* Exclude list for videos.
 *
 * The point of this piece is to make a statement, not to comprehensively
 * document each incident (the original repo is better for that anyway). Videos
 * that are difficult to understand, over-commentated, over-lengthy, or that
 * otherwise detract from the immediate impact of their content should be
 * excluded. Exclusion is not a statement on the accuracy or appropriateness of
 * the video itself. We exclude some videos for the same reason we exclude news
 * articles and images -- not because they're uninformative or unhelpful, but
 * because they don't fit in this specific visualization. */
const EXCLUDE = {
    "srC5l--cn14.mp4": true, /* REASON: unnecessary editorialization */
    "963503949886.mp4": true, /* REASON: way too long (1:36:24) */
    "1267014293628870656.mp4": true, /* REASON: multiple incidents strung together into one video. */
    "66rncy8m1e251.mp4": true, /* REASON: too short, not clear what's happening. */
    "320ea302-9806-44c0-843e-49d8486c423f.mp4": true, /* REASON: duplicate video. */
    "4ix8f3j6dy151.jpg": true, /* REASON: image */
    "1267122343454953472.mp4": true, /* REASON: duplicate */
    "1267974081577717762.mp4": true, /* REASON: destruction of property is bad, but not directed at a person. */
    "wofh339sqr251.png": true, /* REASON: image */
    "4qzvp2gd54251.jpg": true, /* REASON: image */
    "10219444419838711.mp4": true, /* REASON: destruction of property is bad, but not directed at a person. */
    "1268391718086422528.mp4": true, /* REASON: duplicate */
    "1268391718086422528.mp4": true, /* REASON: too long (28 min) -- Consider shortening and re-uploading? */
    "1266850004720812032.mp4": true, /* REASON: duplicate */
    "1266522692326428672.mp4": true, /* REASON: video is entirely after/before incident. */
    "index.html": true, /* REASON: not a video. */
    "9e9323af-3ea1-4d3c-856d-a7f0a66688e7.mp4": true, /* REASON: duplicate */
    "1267588991655784448.mp4": true, /* REASON: duplicate */
    "3091613010894973.mp4": true, /* REASON: video is entirely after/before incident. */
    "8a04afkayz251.mp4": true, /* REASON: duplicate */
    "1266581597760831489.mp4": true, /* REASON: video is entirely after/before incident. */
    "w6gbykiyqg251.mp4": true, /* REASON: duplicate */
    "1266877181164089349.mp4": true, /* REASON: destruction of property is bad, but not directed at a person. */
    "1266557059606163456.mp4": true, /* REASON: duplicate */
    "4ztaywo8fe251.mp4": true, /* REASON: duplicate */
    "1268223118394392576.mp4": true, /* REASON: duplicate */
    "1267203751913422849.mp4": true, /* REASON: duplicate */
    "O3qj4cfsd7g.mp4": true, /* REASON: duplicate */
    "NN8ISwuiX68.mp4": true, /* REASON: unnecessary editorialization */
    "1267118696960528386.mp4": true, /* REASON: duplicate */
    "3020200918026478.mp4": true, /* REASON: unnecessary editorialization */
    "1YqJDEZLdmOxV.mp4": true, /* REASON: too long (28 min) */
    "1266787064735043591.mp4": true, /* REASON: duplicate */
    "1266796191469252610.mp4": true, /* REASON: duplicate */
    "1266878189537824772.mp4": true, /* REASON: duplicate */
    "1266816739444166656.mp4": true, /* REASON: duplicate */
    "1266933807929798656.mp4": true, /* REASON: duplicate */
    "3hv0ks9zo251.mp4": true, /* REASON: duplicate */
    "ges6akxmo9251.jpg": true, /* REASON: image */
    "72pmjn.mp4": true, /* REASON: duplicate */
    "xuu1xy59bl251.mp4": true, /* REASON: duplicate */
    "ns0uj557x0251.jpg": true, /* REASON: image */
    "1267597024096137217.mp4": true, /* REASON: duplicate */
    "1267606685767667712.mp4": true, /* REASON: duplicate */
    "1713b881-a750-45a0-9c6c-f066ebcac484.mp4": true, /* REASON: duplicate */
    "vzm1piy3kn251.mp4": true, /* REASON: duplicate, too short (1 sec repeated 6 times) */
    "l4l3uaqyfl251.gif": true, /* REASON: image */
    "Colorado-Springs-police-issue-statement-following-use-of-force-arrest-during-protest-570969681.mp4": true, /* REASON: duplicate */
    "1266758227930333188.mp4": true, /* REASON: duplicate */

    "1267185423333199874.mp4": true, /* REASON: duplicate (this is the primary source, but the reported version gives better context) */
    "1266634027257098240.mp4": true, /* REASON: duplicate */
    "y94dwbfgya251.mp4": true, /* REASON: duplicate, unnecessary editorialization. */
    "l0yq3023p2251.mp4": true, /* REASON: duplicate */
    "1123349158037544.mp4": true, /* REASON: duplicate */


    /* --------------- Included with explanation --------------  */

    "1266987126467461120.mp4": false, /* REASON: although it's only destruction of property, the destruction is directed at punishing/hurting a person. */
    "100000007175316.mov": false, /* REASON: video is over-editorialized, but without the editors, it's difficult to follow what's happening. The commentary is necessary. */
    "1267974555332685824.mp4": false, /* REASON: destruction of property and video post-dates incident. BUT, incident is described in detail, and destruction of property coincides with arrests/attacks. */
};

/* Videos that show particularly egregious and un-defendable violence.
 *
 * This list is entirely subjective. I'm balancing multiple concerns and there's
 * not a clear cutoff point for what makes a video "partiularly" egregious. I
 * want this to be a relatively small proportion of the entire list. That means
 * that some videos that could arguably be on this list might get cut. But the
 * point is to have a digestible sub-list that shows the worst of the worst.
 *
 * There is no single correct answer to what should and shouldn't be here. */
const HIGHLIGHT = {
    "q356rndwpj251.mp4": true, /* DESC: Philly cops hide behind a wall and beat a civilian. */
    "1267030131563827200.mp4": true, /* DESC: Protestor is hit in the center of the head by a projectile. */
    "1266876680142049282.mp4": true, /* DESC: Police officer attacks, berates civilian, unprovoked: "what the **** are you going to do about it?" */
    "1267305587206807553.mp4": true, /* DESC: Police officer beats man, forcibly places fingers on baton, then beats fingers to remove them. */
    "1268712530358292484.mp4": true, /* DESC: Police shove man to ground, leave him bleeding from ear. */
    "1266885414016688134.mp4": true, /* DESC: Police car partially drives through crowd of protestors. */
    "100000007175316.mov": true, /* DESC: restaurant owner is shot and killed by police. */
    "1268313958479532034.mp4": true, /* DESC: Police assault person on ground during an arrest. */
    "1266945268567678976.mp4": true, /* DESC: Police attack, pepper-spray press while showing press badges. */
    "1266921821653385225.mp4": true, /* DESC: Police shoot woman on her porch. */
    "1266866982919516160.mp4": true, /* DESC: Congresswoman pepper sprayed */
    "6833913925231185158.unknown_video": true, /* DESC: Police refuse to give woman her insulin, arrest driver. */
    "2noh675kou251.mp4": true, /* DESC: Police shoot and critically injure protest, shoot medics trying to give him attention. */
    "1266903223220097024.mp4": true, /* DESC: Police advance on, pepper-spray protestors sitting on the ground. */
    "qf9vf0hdrd251.mp4": true, /* DESC: Police assault obvious news reporter. */
    "4577118.mp4": true, /* DESC: Police attack car, break windows, assault passengers during arrest. */
    "10221345617964005.mp4": true, /* DESC: Police repeatedly punch protestor during arrest. */

    /* "o3v8ps7rat151.mp4": true, /* DESC: police shoots at reporter. */
    /* "1268043720399691776.mp4 DESC: police body-slams protestor. */

    // "1267210841595604992.mp4": true, /* DESC: */
    /* "1266911382613692422.mp4": true, */ /* DESC: Newsreporter describes being attacked. */
    /* xf93fudhv4251.mp4 */
    /* mjm61qrvsg251.mp4 */
    /* wmpic2NU1RM.mp4 */
};

window.onload = async function init () {

    /* TODO: mirror this repository and hit your own API. */
    let response = await fetch('/assets/videos.json', {
        method: 'GET'
    });

    let raw_data = await response.json();

    /* Parse videos into internally used format. */
    let videos = raw_data.data.reduce(function (result, item) {
        item.links.forEach(function (link) {
            if (!link.key || EXCLUDE[link.key]) { return; }
            result[link.key] = {
                key: link.key,
                video: link.spaces_url,
                source: link.link,

                date: new Date(item.date),
                location: item.state,
                description: item.name,
            };
        });

        return result;
    }, {});

    /* Used for looping/progression. */
    /* TODO: wire up autoplay. */
    let video_list = Object.keys(videos).map(function (key) { return videos[key]; });

    /* Switching videos. */
    function loadVideo (id) {
        let video = videos[id];
        let video_elem = document.querySelector('.Video__source');
        video_elem.src = video.video;

        document.querySelectorAll('.Link').forEach(function (link) {
            link.dataset.selected = false;
        });

        /* Update selected item. */
        let selected = document.querySelector('.Link[data-key="' + id + '"]');
        selected.dataset.selected = true;
        selected.dataset.visited = true;
        localStorage.setItem(id, 'true');

        video_elem.play();

        /* Build info popover. */
        let info = h('.Info', { 'data-hidden': true, }, [
            h('p.Info__date', [
              video.date.toLocaleDateString('en-US', { month: 'long', day: 'numeric' }),
              h('a.Info__source', { href: video.source }, '[source]')]),

            h('p.Info__desc', video.description)
        ]);

        let container = document.querySelector('.Video__overlay');
        container.innerHTML = '';
        container.appendChild(info);
    }

    document.querySelector('.Video__source').onpause = function () { showInfo(false); };
    document.querySelector('.Video__source').onplay = function () { showInfo(true); };
    function showInfo (toggle) {
        let container = document.querySelector('.Video__overlay .Info');
        if (container) {
            container.dataset.hidden = toggle;
        }
    }

    window.addEventListener('hashchange', function () {
        let id = location.hash.slice(1);
        loadVideo(id);
    }, false);

    /* Init index */
    let links = video_list.map(function (video, index) {
        let link = h('li.Nav__item', h(
            'a.Link', { href: '#' + video.key,
                        'data-visited': localStorage.getItem(video.key) === 'true',
                        'data-selected': false,
                        'data-index': index,
                        'data-key': video.key,
                        'data-visible': false,
                        'data-highlight': HIGHLIGHT[video.key] },

            h('.Link__label', video.description)));

        return link;
    });

    /* TODO: make this into a more subtle effect. */
    function initDisplay (fast) {

        document.querySelector('.Video__init').dataset.hidden = "true";
        document.querySelector('.Video__playing').dataset.hidden = "false";

        links.forEach(function (link) {
            document.querySelector('.Nav').appendChild(link);
        });

        /* Gradually filling in incidents is important, because it gets across
         * the number. I want people visiting the page to be constantly thinking
         * "when are the dots going to stop appearing?". I want the time it
         * takes to fill in the dots to have an impact. */
        (function next (index) {
            let link = links[index];
            link.querySelector('.Link').dataset.visible = "true";

            if (index === links.length - 1) { return; }
            if (fast) { next(index + 1); return; }
            setTimeout(function () { next(index+1); }, 55);
        }(0));
    }

    /* Wire up init. */
    document.querySelector('.Start').onclick = function () {
        initDisplay(false);
        loadVideo(video_list[0].key);
    };

    /* If you already have a video selected, skip the intro. */
    let current_hash = location.hash.slice(1);
    if (videos[current_hash]) {
        initDisplay(true);
        loadVideo(current_hash);
    }
};
