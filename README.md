# To Protect and Serve

[_To Protect and Serve_](https://to-protect-and-serve.com) is a front-end
display for over 200 videos of police brutality and violence dating back to May
26, 2020 during protests over George Floyd's murder.

Data in this repo was compiled at https://github.com/2020PB/police-brutality,
specifically through the API at https://github.com/nickatnight/policebrutality.io

The data on this site is not being updated automatically, so it's somewhat out
of date. Videos need to be manually looped over so duplicates can be excluded
and particularly egregious examples of police brutality can be highlighted.

# Design Notes and UX Decisions

Documentation required.

# Contributing Guidelines

This site is a work in progress. There are lots of ways to help:

- Locating duplicate and inappropriate videos and excluding them.

- Improving contribution guides.

- Updating the data with new instances (I'm pretty far behind).

- Adding additional features as needed.

# Code of Conduct

The point of this repository is to draw attention to police brutality in a
visceral, indisputable form -- to provide a direct answer to people who claim
that this violence is rare, or that it's perpetuated purely on by individuals or
"bad apples".

What I'm trying to get at is that this repository is not a neutral platform or
community gatheringplace, it is a persuasive essay with a very specific goal.

Racism, sexism, bigotry, or derogitory language of any kind will get you called
out and/or banned. There are places where I'm interested in trying to change
people's views on these subjects, but the issue tracker isn't one of them.
Respect other people's identities, genders/pronouns, religious beliefs, and
experiences.

Issues and pull requests are not the correct place to debate policy issues, and
they're certainly not the place to debate unrelated topics. Again, this
repository is not a neutral platform, it has a singular goal. Where possible,
keep conversation focused on that goal.

There is no single correct way to approach a project like this, some of the
decisions I make about which videos to include, which features to add, and how
to phrase things on the site will be subjective. I'll do my best to openly and
honestly consider feedback whenever I can. I can't promise that every decision I
make will have unanimous consent, but I can promise that I'll do my best.

In the worst case scenario, forks are welcome.

If someone in the issue tracker or in a pull request is making you feel
unwelcome or violating this code of conduct, reach out to me, and I'll do my
best to handle it.
